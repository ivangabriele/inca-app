import styled from 'styled-components'

export const Page = styled.div`
  display: flex;
  flex-grow: 1;
  max-width: 100%;
`
