import styled from 'styled-components'

export const Main = styled.main`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  max-height: 100%;
  max-width: 100%;
  min-width: 0;
  overflow-y: auto;
  padding: 1rem 1rem 1rem 17rem;
  width: 100%;
`
